function MyDiv(){
    $("div.first").hover(function(){
        $('#two,#three,#four,#five,#six,#seven,#eight,#nine,#ten').hide();
        $("#content").append("<div id='one'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>Home is Just a Click Away </h1> <p class='animated fadeInUp'>Shop for your family in Zimbabwe<br/> form the Diaspora</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner0.png)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.sec").hover(function(){
        $('#one,#three,#four,#five,#six,#seven,#eight,#nine,#ten').hide();
        $("#content").append("<div id='two'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>College Press </h1> <p class='animated fadeInUp'>Some text here about College Press<br/>something here also</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner3.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.third").hover(function(){
        $('#one,#two,#four,#five,#six,#seven,#eight,#nine,#ten').hide();
        $("#content").append("<div id='three'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>ZFC Limited</h1> <p class='animated fadeInUp'>Helping you get a better crop<br/>Best fertilizers Best Results</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner4.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.forth").hover(function(){
        $('#one,#two,#three,#five,#six,#seven,#eight,#nine,#ten').hide();
        $("#content").append("<div id='four'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>DHAKA </h1> <p class='animated fadeInUp'>DecorVases<br/>some text if possible</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner5.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.fifth").hover(function(){
        $('#one,#two,#three,#four,#six,#seven,#eight,#nine,#ten').hide();
        $("#content").append("<div id='five'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>NetOne</h1> <p class='animated fadeInUp'>The World in One<br/>Experience Extraordinary</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner1.png)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.sixth").hover(function(){
        $('#one,#two,#three,#four,#five,#seven,#eight,#nine,#ten').hide();
        $("#content").append("<div id='six'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>DORI</h1> <p class='animated fadeInUp'>For Quality<br/>Genuine Leather </p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner6.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.seventh").hover(function(){
        $('#one,#two,#three,#four,#five,#six,#eight,#nine,#ten').hide();
        $("#content").append("<div id='seven'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>Mega Pack </h1> <p class='animated fadeInUp'>some text here and<br/>this text is a place holder</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner7.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.eight").hover(function(){
        $('#one,#two,#three,#four,#five,#six,#seven,#nine,#ten').hide();
        $("#content").append("<div id='eight'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>Health and Beauty</h1> <p class='animated fadeInUp'>Some place holder text <br/>About Healthy and Beauty</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner8.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.nine").hover(function(){
        $('#one,#two,#three,#four,#five,#six,#seven,#eight,#ten').hide();
        $("#content").append("<div id='nine'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>Music World</h1> <p class='animated fadeInUp'>Some place holder text <br/>About Healthy and Beauty</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner9.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg)");
    });

    $("div.ten").hover(function(){
        $('#one,#two,#three,#four,#five,#six,#seven,#nine,#eight').hide();
        $("#content").append("<div id='ten'><div class='row margin-page'><span id='banner-info' class='animated fadeInDown'>&copy2018, Hammer and Tongues</span> <div class='col-md-12' > <h1 class='banner-head animated fadeInUp'>Ford </h1> <p class='animated fadeInUp'>Cars for sale<br/>About Healthy and Beauty</p></div></div> </div>");
        $("div#other").css("background-image", "url(img/Banner/banner10.jpg)");
        }, function(){
        $("div#other").css("background-image", "url(img/Banner/banner2.jpg");
    });

}



// function show(id) {
//     document.getElementById(id).style.visibility = "visible";
// }

// function hide(id) {
//     document.getElementById(id).style.visibility = "hidden";
// }
   

//function for my overlay to make text appear on top